#include <iostream>
#include <fstream>
#include <cstdio>
#include <vector>
#include <performance/timer.h>

#include <cub/cub.cuh>
#include <cub/util_type.cuh>
#include <cub/block/block_scan.cuh>

#include <petscksp.h>
#include <petsctime.h>
#include <petscmat.h>

#include <cusp/csr_matrix.h>
#include <cusp/gallery/diffusion.h>
#include <cusp/gallery/poisson.h>
#include <cusp/krylov/cg.h>
#include <cusp/krylov/gmres.h>
#include <cusp/precond/aggregation/smoothed_aggregation.h>
#include <cusp/precond/diagonal.h>
#include <cusp/precond/smoother/gauss_seidel_smoother.h>
#include <cusp/precond/smoother/polynomial_smoother.h>

#include "log_times.hpp"
#include "argh.h"

PetscInt readPETSC(const char* fname, std::vector<int> &rows, std::vector<int> &cols, std::vector<double> &vals, std::vector<double> &rhs){
    int N, n_rows;

    std::ifstream fin(fname, std::ios::in | std::ios::binary);
    // std::ifstream fin(fname.c_str(), std::ios::in | std::ios::binary);
    fin.read((char*)&N, sizeof(int));
    rows.resize(N);
    cols.resize(N);
    vals.resize(N);
    fin.read((char*)rows.data(), N * sizeof(int));
    fin.read((char*)cols.data(), N * sizeof(int));
    fin.read((char*)vals.data(), N * sizeof(double));

    fin.read((char*)&n_rows, sizeof(int));
    rhs.resize(n_rows);
    fin.read((char*)rhs.data(), n_rows * sizeof(double));

    fin.close();

    return n_rows;
}


static char help[] = "Reads a PETSc matrix and vector from a file\n";


int cusp_solve(const int* const row, const int* const col, const double * const val, size_t N,
	       const double* const RHS, const double* X, size_t n) {
        
    	typedef cusp::device_memory mem_space;
	timer t0;
        std::cout << "Allocate device memory for CSR format\n";
        int   * device_I;
        cudaMalloc(&device_I, N * sizeof(int));
        int   * device_J;
        cudaMalloc(&device_J, N * sizeof(int));
        double * device_V;
        cudaMalloc(&device_V, N * sizeof(double));

        std::cout << "Allocate device memory for x and y arrays\n";
        double * device_x;
        cudaMalloc(&device_x, n * sizeof(double));
        double * device_b;
        cudaMalloc(&device_b, n * sizeof(double));
        
        std::cout << "Copy raw data from host to device\n";
        cudaMemcpy(device_I, row, N * sizeof(int),   cudaMemcpyHostToDevice);
        cudaMemcpy(device_J, col, N * sizeof(int),   cudaMemcpyHostToDevice);
        cudaMemcpy(device_V, val, N * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(device_b, RHS, n * sizeof(double), cudaMemcpyHostToDevice);

        std::cout << "Matrices and vectors now reside on the device\n";
        thrust::device_ptr<int>   wrapped_device_I(device_I);
        thrust::device_ptr<int>   wrapped_device_J(device_J);
        thrust::device_ptr<double> wrapped_device_V(device_V);
        thrust::device_ptr<double> wrapped_device_b(device_b);

        std::cout << "Use array1d_view to wrap the individual arrays\n";
        typedef typename cusp::array1d_view< thrust::device_ptr<int>   > DeviceIndexArrayView;
        typedef typename cusp::array1d_view< thrust::device_ptr<double> > DeviceValueArrayView;
        DeviceIndexArrayView row_indices   (wrapped_device_I, wrapped_device_I + N);
        DeviceIndexArrayView column_indices(wrapped_device_J, wrapped_device_J + N);
        DeviceValueArrayView values        (wrapped_device_V, wrapped_device_V + N);
        DeviceValueArrayView b             (wrapped_device_b, wrapped_device_b + n);        

        cusp::array1d<double, mem_space> x(n, 0);

        std::cout << "Combine the three array1d_views into a coo_matrix_view\n";
        typedef cusp::coo_matrix_view<DeviceIndexArrayView,
            DeviceIndexArrayView,
            DeviceValueArrayView> DeviceView;

        std::cout << "Construct a coo_matrix_view from the array1d_views\n";
        DeviceView A(n, n, N, row_indices, column_indices, values);

        std::cout << "Set up preconditioner\n";
        // cusp::precond::smoothed_aggregation<int, double, cusp::device_memory> M(A);
        cusp::precond::diagonal<double, cusp::device_memory> M(A);

        double tm_setup = t0.seconds_elapsed();

        double tol = 1e-5;
        // cmdl({"e", "tol"}, "1e-5") >> tol;
        cusp::monitor<double> monitor(b, 1000, tol);

        // solve
        timer t1;
        int restart = 250;
        // cusp::krylov::cg(A, x, b, monitor, M);
        cusp::krylov::gmres(A, x, b,restart, monitor, M);

        double tm_solve = t1.seconds_elapsed();

        std::cout
            << "iters: " << monitor.iteration_count()    << std::endl
            << "error: " << monitor.relative_tolerance() << std::endl
            << "setup: " << tm_setup << std::endl
            << "solve: " << tm_solve << std::endl
            ;
    	return 0;
}


//---------------------------------------------------------------------------
int main(int argc,char **argv) {
    PetscInt           range;
    PetscBool          flg_file, flg_range;
    PetscLogDouble     tic, toc;
    KSPConvergedReason reason;
    char               file_str[PETSC_MAX_PATH_LEN];   /* input file name full path */
    char               file_base[PETSC_MAX_PATH_LEN];  /* input file name base path */
    char               index_str[10];                  /* input file name index*/

    argh::parser cmdl(argc, argv);

    typedef cusp::device_memory mem_space;

    std::vector<int> ptr, col;
    std::vector<double> val, rhs;

    PetscInitialize(&argc,&argv,(char*)0,help);
    PetscOptionsGetString(NULL,NULL,"-fbase",file_base,sizeof(file_base),&flg_file);
    PetscOptionsGetInt(NULL,NULL,"-frange",&range,&flg_range);

    if (!flg_file) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Must indicate binary file path (without terminating number) with -fbase option");
    if (!flg_range) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Must indicate range of binary files with the -frange option");

    for (int i = 0; i <= range; ++i)
    {
        snprintf(file_str, sizeof(file_str), "%s%d\0", file_base, i);
        PetscPrintf(PETSC_COMM_WORLD, file_str);

        std::vector<int> row, col;
        std::vector<double> val, rhs, X;

        int n = readPETSC(file_str, row, col, val, rhs);
	
	X.resize(n);
	
	cusp_solve(row.data(), col.data(), val.data(), row.size(), rhs.data(), X.data(), n);
	
	return;
        timer t0;
        std::cout << "Allocate device memory for CSR format\n";
        int   * device_I;
        cudaMalloc(&device_I, row.size() * sizeof(int));
        int   * device_J;
        cudaMalloc(&device_J, col.size() * sizeof(int));
        double * device_V;
        cudaMalloc(&device_V, val.size() * sizeof(double));

        std::cout << "Allocate device memory for x and y arrays\n";
        double * device_x;
        cudaMalloc(&device_x, n * sizeof(double));
        double * device_b;
        cudaMalloc(&device_b, n * sizeof(double));
        
        std::cout << "Copy raw data from host to device\n";
        cudaMemcpy(device_I, row.data(), row.size() * sizeof(int),   cudaMemcpyHostToDevice);
        cudaMemcpy(device_J, col.data(), col.size() * sizeof(int),   cudaMemcpyHostToDevice);
        cudaMemcpy(device_V, val.data(), val.size() * sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(device_b, rhs.data(), rhs.size() * sizeof(double), cudaMemcpyHostToDevice);

        std::cout << "Matrices and vectors now reside on the device\n";
        thrust::device_ptr<int>   wrapped_device_I(device_I);
        thrust::device_ptr<int>   wrapped_device_J(device_J);
        thrust::device_ptr<double> wrapped_device_V(device_V);
        thrust::device_ptr<double> wrapped_device_b(device_b);

        std::cout << "Use array1d_view to wrap the individual arrays\n";
        typedef typename cusp::array1d_view< thrust::device_ptr<int>   > DeviceIndexArrayView;
        typedef typename cusp::array1d_view< thrust::device_ptr<double> > DeviceValueArrayView;
        DeviceIndexArrayView row_indices   (wrapped_device_I, wrapped_device_I + row.size());
        DeviceIndexArrayView column_indices(wrapped_device_J, wrapped_device_J + col.size());
        DeviceValueArrayView values        (wrapped_device_V, wrapped_device_V + val.size());
        DeviceValueArrayView b             (wrapped_device_b, wrapped_device_b + rhs.size());        

        cusp::array1d<double, mem_space> x(n, 0);

        std::cout << "Combine the three array1d_views into a coo_matrix_view\n";
        typedef cusp::coo_matrix_view<DeviceIndexArrayView,
            DeviceIndexArrayView,
            DeviceValueArrayView> DeviceView;

        std::cout << "Construct a coo_matrix_view from the array1d_views\n";
        DeviceView A(n, n, row.size(), row_indices, column_indices, values);

        std::cout << "Set up preconditioner\n";
        // cusp::precond::smoothed_aggregation<int, double, cusp::device_memory> M(A);
        cusp::precond::diagonal<double, cusp::device_memory> M(A);

        double tm_setup = t0.seconds_elapsed();

        double tol = 1e-5;
        // cmdl({"e", "tol"}, "1e-5") >> tol;
        cusp::monitor<double> monitor(b, 1000, tol);

        // solve
        timer t1;
        int restart = 250;
        // cusp::krylov::cg(A, x, b, monitor, M);
        cusp::krylov::gmres(A, x, b,restart, monitor, M);

        double tm_solve = t1.seconds_elapsed();

        std::cout
            << "iters: " << monitor.iteration_count()    << std::endl
            << "error: " << monitor.relative_tolerance() << std::endl
            << "setup: " << tm_setup << std::endl
            << "solve: " << tm_solve << std::endl
            ;
    }

    PetscFinalize();
    return 0;
}




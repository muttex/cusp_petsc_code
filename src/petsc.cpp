#include <iostream>
#include <fstream>
#include <cstdio>
#include <vector>
#include <petscksp.h>
#include <petsctime.h>
#include <petscmat.h>

#include "log_times.hpp"


static char help[] = "Reads a PETSc matrix and vector from a file\n";

//---------------------------------------------------------------------------
int main(int argc,char **argv) {
    Mat                A;
    Vec                v, x;
    KSP                solver;
    PC                 prec;
    PetscViewer        viewer;                        /* viewer */
    PetscBool          flg_file, flg_range;
    PetscLogDouble     tic, toc;
    KSPConvergedReason reason;
    char               file_str[PETSC_MAX_PATH_LEN];   /* input file name full path */
    char               file_base[PETSC_MAX_PATH_LEN];  /* input file name base path */
    char               index_str[10];                  /* input file name index*/
    PetscInt           range;
    PetscInt           iters;
    PetscReal          error;

    PetscInitialize(&argc,&argv,(char*)0,help);
    /*
    Determine files from which we read the two linear systems
    (matrix and right-hand-side vector).
    */
    PetscOptionsGetString(NULL,NULL,"-fbase",file_base,sizeof(file_base),&flg_file);
    PetscOptionsGetInt(NULL,NULL,"-frange",&range,&flg_range);

    if (!flg_file) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Must indicate binary file path (without terminating number) with -fbase option");
    if (!flg_range) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Must indicate range of binary files with the -frange option");

    for (int i = 0; i <= range; ++i)
    {
        snprintf(file_str, sizeof(file_str), "%s%d\0", file_base, i);
        PetscPrintf(PETSC_COMM_WORLD, file_str);

        PetscViewerBinaryOpen(PETSC_COMM_WORLD,file_str,FILE_MODE_READ,&viewer);
        // PetscViewerBinaryOpen(PETSC_COMM_WORLD,"../MatrixData/41/data_0",FILE_MODE_READ,&viewer);

        MatCreate(PETSC_COMM_WORLD,&A);
        MatSetType(A,MATMPIAIJ);
        MatLoad(A,viewer);
        VecCreate(PETSC_COMM_WORLD,&v);
        VecLoad(v,viewer);
        VecDuplicate(v,&x);    

        PetscTime(&tic);
        KSPCreate(MPI_COMM_WORLD, &solver);
        KSPSetOperators(solver,A,A);
        // KSPSetTolerances(solver, 1e-8, PETSC_DEFAULT, PETSC_DEFAULT, 10000);
        KSPSetType(solver, KSPGMRES);
        KSPGMRESSetRestart(solver, 250);
        KSPGetPC(solver,&prec);
        PCSetType(prec, PCJACOBI);
        KSPSetFromOptions(solver);
        PCSetFromOptions(prec);
        KSPSetUp(solver);
        PetscTime(&toc);
        double tm_setup = toc - tic;

        PetscTime(&tic);
        KSPSolve(solver,v,x);
        PetscTime(&toc);
        double tm_solve = toc - tic;

        KSPGetConvergedReason(solver,&reason);
        if (reason==KSP_DIVERGED_INDEFINITE_PC) {
            PetscPrintf(PETSC_COMM_WORLD,"\nDivergence because of indefinite preconditioner;\n");
            PetscPrintf(PETSC_COMM_WORLD,"Run the executable again but with '-pc_factor_shift_type POSITIVE_DEFINITE' option.\n");
        } else if (reason<0) {
            PetscPrintf(PETSC_COMM_WORLD,"\nOther kind of divergence: this should not happen.\n");
        } else {
            KSPGetIterationNumber(solver,&iters);
            KSPGetResidualNorm(solver,&error);
            PetscPrintf(PETSC_COMM_WORLD,"\niters: %d\nerror: %lf", iters, error);
            PetscPrintf(PETSC_COMM_WORLD,"\nsetup: %lf\nsolve: %lf\n\n", tm_setup, tm_solve);
        }

    }
    // MatView(A,PETSC_VIEWER_STDOUT_WORLD);
    MatDestroy(&A);
    VecDestroy(&v);
    VecDestroy(&x);
    KSPDestroy(&solver);
    PetscViewerDestroy(&viewer);

    PetscFinalize();
    return 0;
}
